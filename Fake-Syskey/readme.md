# Modified Fake Syskey
This version comes from:
https://github.com/pazuzu156/Fake-Syskey

However, I modified it to remove the 'twist' message and replaced it by the normal message. The purpose is to give a false sense of victory to the scammer without alerting him something is wrong.
So in this case, if this is the first thing the scammer tries, you would still be able to get to him.

# Fake Syskey
Scammers are well known for turning on syskey on their victim's computers. Since we know better, we don't want them to have access to this powerful tool of destruction (if a scammer is using it that is).

This little applet is a "replacement" fake version of syskey, in that it looks and acts like the real deal, with a twist!

## Building
To build the app, you need Visual Studio. This is built in 2013 Pro, but any version of VS from 2013+ will work. Simply run `build.bat` and follow the instructions.

## Installing
To install, go to `C:\Windows\System32` and rename syskey.exe to syskey.exe.bak (so you still have the real thing, don't wannt remove it do we?) and paste the new syskey.exe in it's place.

Keep in mind, you need your account to have full access, otherwise, you'll get an error needing the "TrustedInstaller" to allow access to it. Just change the file's permissions. (Be sure to change them back once you're done as to not leave the file open for others to screw with!)

## Message Creator
Message Creator is built in WPF, so it looks and works a bit different than WinForms. It also requires the UniversalControls.dll AND syskey.exe in the same directory to work. syskey.exe however, can operate independent of both the dll and Message Creator, so if all you want is syskey.exe, then go for it!

**DISCLAIMER**
While I'm using the public URL for cloning THIS repository, it's best to fork it and clone your fork. It'll still be able to clone the sub module, but this way you can push any changes you make and easily make a pull request.

## Uninstalling
To remove, go to `C:\Windows\System32` and delete the fake syskey, and rename syskey.exe.bak (assuming you did as I commanded....) to syskey.exe. Now, you're back in business!

## Downloads
Downloads are hosted here on [good 'ol Github](https://github.com/pazuzu156/Fake-Syskey/releases)
