# Anti-Scam Tools

# Kill Process
killProcess is a program that will kill AcroRd32.exe and FoxitReader.exe.
It will start as a service, so very hard for them to detect, and will kill Acrobat Reader and Foxit Reader when starting.

Here the goal is let them think we cannot open a pdf (like banking statement), and we send it to them, so they open it on their computer.
If the pdf has been modified to include some RAT, then the RAT will open on their computer :)

If you want more process to be denied, you will need to download source code, and change the list of process, and recompile the program.

However, it is still possible for them to open the pdf with Chrome. You could add Chrome to the list of denied process, but that might look suspicious.

## Installation
If you want to build from sources:
* Clone this repository
* Edit killProcess_win32.cpp
* Compile it (cl.exe killProcess_win32.cpp) (require Visual Studio to be installed)

Next you need to install Windows Resource Kit on the VM, from here:
https://www.microsoft.com/en-ca/download/details.aspx?id=17657

To install the service you need to:
* Copy killProcess_win32.exe (the compiled one or the one from the repository) to C:\Program Files (x86)\Windows Resource Kits\Tools\
* Run install.bat with administrative privileges (this will create a service based on killProcess_win32.exe)
* Run start_service.bat also with administrative privileges

If you want to stop the service, execute stop_service.bat with administrative privileges
And if you want to completely uninstall it, execute uninstall.bat with administrative privileges

Now, you should be able to open a PDF with Acrobat Reader or Foxit Reader (while the service is running).
The service is identified as RtcLogon (but it can be changed from the batch files to you liking).

# Fake Syskey
Fake Syskey will replace syskey.exe with an exact same version, but that does not encrypt, and save the password entered instead.
It might be useful if they reuse their passwords :)

The saved password is stored in C:\Users\yourusername\AppData\Roaming\fake syskey\syskey_scammers_password.txt

This version is from: https://github.com/pazuzu156/Fake-Syskey
except the twist message has been replaced with the usual message, to avoid raising suspicion on the scammer.

## Installation
Source code can be downloaded and compiled using Visual Studio, or the binary can directly be used as is.