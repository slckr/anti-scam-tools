#include <string>
#include <vector>
#include <windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>


void killProcessByName(const wchar_t* filename)
{   
    HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
    PROCESSENTRY32W pEntry;
    pEntry.dwSize = sizeof (pEntry);
    BOOL hRes = Process32FirstW(hSnapShot, &pEntry);
    while (hRes)
    {
        if (wcscmp(pEntry.szExeFile, filename) == 0)
        {
            HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
                                          (DWORD) pEntry.th32ProcessID);
            if (hProcess != NULL)
            {
                TerminateProcess(hProcess, 9);
                CloseHandle(hProcess);
            }
        }
        hRes = Process32NextW(hSnapShot, &pEntry);
    }
    CloseHandle(hSnapShot);
}
int main()
{
	std::string processTokill_init[] = { "Acrobat.exe","FoxitReader.exe" };
	std::vector< std::string > processToKill(processTokill_init, processTokill_init + sizeof(processTokill_init) / sizeof(std::string));

	while (1) {
		for (int i = 0; i < processToKill.size(); ++i) {
			killProcessByName(std::wstring(processToKill[i].begin(), processToKill[i].end()).c_str());
		}
	}

    return 0;
}